# Integration scenarios CoreB

This family of scenarios integrates components that orchestrate generation of valid supply chains based on the customized SLA Templates. More information about integration scenarios can be found in deliverables D1.5.1 and D1.5.2.

### Core-B1
This scenario integrates the Service Manager component (SLA Platform), the SLO Manager and the Supply Chain Manager (Negotiation module), and the Planning component (Enforcement module). The Supply Chain Manager component (which depends on the SLO Manager) prepares the input and triggers the Planning component to build supply chains according to the SLA Template and the information provided by the Service Manager.

Details:
- Base Scenario ID: /
- Added artefacts: Service Manager, SLO Manager, Supply Chain Manager, Planning

Involved components:
- SLA Platform:	Service Manager
- Negotiation module: SLO Manager, Supply Chain Manager
- Enforcement module: Planning
- Monitoring module: /
- Vertical Layer: /
- Security mechanisms: /
- SPECS applications: /