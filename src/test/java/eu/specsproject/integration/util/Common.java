package eu.specsproject.integration.util;

public class Common {
    public static String ipAddress = "194.102.62.222";
    
    static public String getSLO_MANAGER_BASE_PATH() {
        return "http://"+ipAddress+":8080/slo-manager-api";
    }

    static public String getSLO_MANAGER_TEMPLATE_PATH() {
        return getSLO_MANAGER_BASE_PATH()+"/sla-negotiation/sla-templates";
    }
    
    static public String getSLO_MANAGER_OFFERS_PATH(String slaId) {
        return getSLO_MANAGER_BASE_PATH()+"/sla-negotiation/sla-templates/"+slaId+"/slaoffers";
    }

    static public String getTEMPLATE_NAME() {
        return "SECURE_WEB_CONTAINER";
    }

    static public String getTEMPLATE_PATH() {
        return "src/test/resources/specs_template_nist.xml";
    }

}
