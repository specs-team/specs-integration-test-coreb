package eu.specsproject.integration.test;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.ws.rs.core.MediaType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import eu.specsproject.integration.util.Common;

public class IntegrationPlanTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("setUpBeforeClass Called");
        String ipAddress = System.getenv("IP_ADDRESS");
        System.out.println("Ip Address: "+ipAddress);
        if(ipAddress != null){
            Common.ipAddress = ipAddress;
        }
        deleteTemplateFromSloManagerNoTest();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("tearDownAfterClass Called");
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("setUp Called");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("tearDown Called");
    }


    @Test
    public final void testUploadNewTemplate() {
        ClientConfig cc = new DefaultClientConfig();
        Client client = Client.create(cc);
        try {
            WebResource wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH());
            addTemplateToSloManager(wr, client);

            wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH()+"/"+Common.getTEMPLATE_NAME());
            ClientResponse response = wr.post(ClientResponse.class);
            Assert.assertEquals(200, response.getStatus());
            String responseString = response.getEntity(String.class);
            String slaTemplate = responseString;
            System.out.println(responseString);
            String slaId = getWsagNameFromSla(responseString);
            
            wr = client.resource(Common.getSLO_MANAGER_OFFERS_PATH(slaId));
            ClientResponse response2 = wr.type(MediaType.TEXT_XML).post(ClientResponse.class, slaTemplate);
            
            System.out.println("Response offers: "+response2.getEntity(String.class));
            
            Assert.assertEquals(201, response2.getStatus());
            
            deleteTemplateFromSloManager(client, wr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //UTILITIES FUNCTIONS
    private void addTemplateToSloManager(WebResource wr, Client client) throws IOException{
        String nistTemplate = readFile(Common.getTEMPLATE_PATH());
        wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH());
        ClientResponse response = wr.type(MediaType.TEXT_XML).post(ClientResponse.class, nistTemplate);
        String resposeBody = response.getEntity(String.class);
        System.out.println(resposeBody);
        Assert.assertEquals(201, response.getStatus());
        Assert.assertEquals("\""+Common.getSLO_MANAGER_TEMPLATE_PATH()+"/"+Common.getTEMPLATE_NAME()+"\"", resposeBody);
    }

    private static void deleteTemplateFromSloManagerNoTest(){
        ClientConfig cc = new DefaultClientConfig();
        Client client = Client.create(cc);
        WebResource wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH());
        wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH()+"/"+Common.getTEMPLATE_NAME());
        ClientResponse response = wr.type(MediaType.TEXT_XML).delete(ClientResponse.class);
        System.out.println("First delete response: "+response.getStatus());
    }

    private void deleteTemplateFromSloManager(Client client, WebResource wr){
        wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH()+"/"+Common.getTEMPLATE_NAME());
        ClientResponse response = wr.type(MediaType.TEXT_XML).delete(ClientResponse.class);
        Assert.assertEquals(204, response.getStatus());
    }

    private static String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }
    
    public static String getWsagNameFromSla(String slaDocument){
        String slaId = "none";
        if(slaDocument != null && slaDocument.startsWith("<")){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            StringBuilder xmlStringBuilder = new StringBuilder();
            xmlStringBuilder.append(slaDocument);
            ByteArrayInputStream input =  new ByteArrayInputStream(xmlStringBuilder.toString().getBytes("UTF-8"));
            Document doc = builder.parse(input);
            NodeList nodeList = doc.getElementsByTagName("wsag:Name");
            if(nodeList.getLength() > 0){
                slaId = nodeList.item(0).getTextContent();
            }
            System.out.println("SLA ID: "+slaId);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        }

        return slaId;
    }
}
